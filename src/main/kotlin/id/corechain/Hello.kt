package id.corechain

import com.mashape.unirest.http.Unirest
import org.apache.commons.codec.digest.DigestUtils
import org.openqa.selenium.*
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.support.ui.Select
import java.util.*
import redis.clients.jedis.Jedis
import redis.clients.jedis.exceptions.JedisConnectionException
import java.io.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class WebDriver(){
    fun getWebDriver(): WebDriver? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        var driver: WebDriver
        System.setProperty("webdriver.chrome.driver", props.get("webdriver.chrome.driver").toString())
        driver = ChromeDriver()
        return driver
    }
    fun getDBName(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("db.name").toString()
    }
    fun getPostUrl(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("post.url").toString()
    }
    fun getRedisParam(): HashMap<String,String>? {
        val map:HashMap<String,String> = HashMap<String,String>()
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        map.put("ip",props.get("redis.name").toString())
        map.put("port",props.get("redis.port").toString())
        map.put("password",props.get("redis.password").toString())
        map.put("db",props.get("redis.db").toString())
        return map
    }
    fun directory(): String? {
        val props = Properties()
        val input = FileInputStream("/etc/service.conf");
        props.load(input)
        return props.get("directory.download").toString()
    }
}

fun main(args: Array<String>) {
    var result:String
    try {
        result= checkSaldo("Adryan2707","070116")
    }catch (e:JedisConnectionException){
        System.out.println("connection redis error")
        e.printStackTrace()
    }catch (e:Exception){
        e.printStackTrace()
        id.corechain.WebDriver().getWebDriver()!!.quit()
    }

    return
}

fun checkSaldo(username:String, password:String):String{
    var redisParam:HashMap<String,String> = id.corechain.WebDriver().getRedisParam() as HashMap<String, String>
    val jedis = Jedis(redisParam.get("ip"), redisParam.get("port")!!.toInt())
    if(!redisParam.get("password").equals("")){
        jedis.auth(redisParam.get("password"))
    }
    jedis.select(redisParam.get("db")!!.toInt())
    var result =""
    val webDriver =id.corechain.WebDriver().getWebDriver()
    if(webDriver!=null){
        var error =false
        var alertError =false
        try{
            webDriver.get("https://ib.bankmandiri.co.id/retail/Login.do?action=form")
            var element = webDriver.findElement(By.name("userID"))
            element.sendKeys(username)
            element = webDriver.findElement(By.name("password"))
            element.sendKeys(password)
            element.submit()

            webDriver.switchTo().defaultContent()
            var frame = webDriver.findElement(By.name("toc"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("Informasi Rekening")).click()
            webDriver.findElement(By.linkText("Rekening Tabungan dan Giro")).click()

            jedis.set("Mandiri:lastLogin",Date().time.toString())

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("CONTENT"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("id('saSelect')/option[2]")).click();
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("CONTENT"))
            webDriver.switchTo().frame(frame)
            var balance =webDriver.findElement(By.xpath("//table[@cellspacing='1']/tbody/tr//td[@id='accbal']")).text
            var saldo = balance.replace("Rp. ","")
            saldo = saldo.replace(".","")
            var now =System.currentTimeMillis() / 1000L
            result=result+"{\"bank\":\"mandiri\",\"amount\":\""+saldo+"\",\"lastupdate\":\""+now+"\"}"
            jedis.set(username+":Mandiri:"+now,result)
            jedis.set(username+":Mandiri:lastest",result)

            result ="result :["
            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("toc"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("Mutasi Rekening")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("CONTENT"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.xpath("//td//input[@value='L']")).click()
            val sel = Select(webDriver.findElement(By.xpath("//select[@name='lastTransaction']")))
            sel.selectByIndex(2)
            (webDriver as JavascriptExecutor)
                    .executeScript("formSubmit();")

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("CONTENT"))
            webDriver.switchTo().frame(frame)

            webDriver.findElement(By.xpath("//a//img[@src='images/button-download.gif']")).click()

            webDriver.switchTo().defaultContent()
            frame = webDriver.findElement(By.name("top"))
            webDriver.switchTo().frame(frame)
            webDriver.findElement(By.linkText("LOGOUT")).click()

            TimeUnit.SECONDS.sleep(5)
            var directory = id.corechain.WebDriver().directory()
            val folder = File(directory + "/")
            val listOfFiles = folder.listFiles()
            for (i in listOfFiles.indices) {
                if (listOfFiles[i].isFile()) {
                    if (listOfFiles[i].getName().contains(".csv")) {
                        var nameFile:String =""+Date().time+listOfFiles[i].getName()
                        listOfFiles[i].renameTo(File(directory + "/mutasi/" +  nameFile+ ""))
                        listOfFiles[i].delete()
                        val csvFile = directory + "/mutasi/" + nameFile + ""
                        val cvsSplitBy = ","
                        var counter = 0
                        try {
                            BufferedReader(FileReader(csvFile)).use { br ->
                                var indicator = false
                                var test = true;
                                while (test) {
                                    counter++
                                    val line = br.readLine()
                                    if(line==null){
                                        break
                                    }
                                    if (counter == 13 || indicator == true) {
                                        var splittedLine = line.split(",")
                                        if (splittedLine[0].equals("No Record Found") || splittedLine[0].equals("")) {
                                            break
                                        } else {
                                            try {
                                                var res = ""
                                                var debitIndicator =false
                                                indicator = true
                                                var splitDate = splittedLine[0].replace("\"","").split("/")
                                                var date=""
                                                date = ""+ splitDate[2]+splitDate[1]+splitDate[0]
                                                var type = ""
                                                var amount =0
                                                if(splittedLine[2].equals("\"0")){
                                                    amount = splittedLine[4].replace("\"", "").replace(".","").toInt()
                                                }else{
                                                    debitIndicator =true
                                                    amount = splittedLine[2].replace("\"", "").replace(".","").toInt()
                                                }
                                                res = res + "{\"Tanggal\":\"" + date + "\","
                                                res = res + "\"Keterangan Transaksi\":\"" + splittedLine[1] + "\","

                                                if (!debitIndicator) {
                                                    type = "in"
                                                    res = res + "\"Debet\":\"" + 0 + "\","
                                                    res = res + "\"Kredit\":\"" + amount + "\"}"
                                                } else {
                                                    type = "out"
                                                    res = res + "\"Debet\":\"" + amount + "\","
                                                    res = res + "\"Kredit\":\"" + 0 + "\"}"
                                                }

//                                                if(jedis.get("mandiri:"+sha1converter(res)) == null){
//                                                    jedis.set("mandiri:"+sha1converter(res),sha1converter(res))
                                                    val postResponse = Unirest.post(id.corechain.WebDriver().getPostUrl())
                                                          .header("accept", "application/json")
                                                          .header("Content-Type", "application/json")
                                                          .body(res).asJson()
//                                                }else{
//                                                    println("mandiri:"+sha1converter(res))
//                                                }

//                                                val r: RethinkDB = RethinkDB.r;
//                                                val conn: Connection = r.connection().hostname("rth0.corechain.id").port(28015).db(id.corechain.WebDriver().getDBName()).user("digiro", "digiro").connect()
//                                                r.table("mutasi").insert(r.hashMap("id", sha1converter(res)).with("date", date.toInt()).with("desc", splittedLine[1]).with("type", type).with("amount", amount).with("processed", -1).with("bank", "BCA")
//                                                ).run<String>(conn)
//                                                conn.close()

//

                                            } catch (e: IndexOutOfBoundsException) {
                                                e.printStackTrace()
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                        break
                    }
                }
            }
        }catch (e:NullPointerException){
            print("Directory not found")
            error=true
        }catch (e: java.util.NoSuchElementException){
            print("Problem")
            error=true
        }catch (e:JedisConnectionException){
            e.printStackTrace()
            System.out.println("connection redis error")
            error=true
        }catch (e: UnhandledAlertException){
            print("alert exception")
            alertError=true
            error=true
        }catch (e:Exception){
            e.printStackTrace()
            error=true
        }finally {
            if(alertError){
                val alert = webDriver.switchTo().alert()
                alert.dismiss()
            }
            if(error){
                webDriver.switchTo().defaultContent()
                var frame = webDriver.findElement(By.name("top"))
                webDriver.switchTo().frame(frame)
                webDriver.findElement(By.linkText("LOGOUT")).click()
                TimeUnit.SECONDS.sleep(5)
            }
            jedis.quit()
            webDriver.quit()
        }
    }
    return result;
}

fun sha1converter(param: String): String {
    return DigestUtils.sha1Hex(param)
}